# Benchmarking the Debian-like project
## Introduction
This repo is essentially just a CI job which runs weekly on a configured runner.
The runner is being used to benchmark the
[Debian-like project (branch: jennis/add_all_fules)](https://gitlab.com/BuildStream/benchmarking/debian-stretch-bst/tree/jennis/add_all_files).

## The benchmarks
The job is scheduled for every Sunday night at 23:59 (see
[here](https://gitlab.com/BuildStream/benchmarking/benchmark_debian/pipeline_schedules)),
where it will run benchmarks for:

* `bst show base-files/base-files.bst`
* `bst --builders 4 build base-files/base-files.bst`
* `bst --builders 8 build base-files/base-files.bst`
* `bst --builders 12 build base-files/base-files.bst`
* `bst show base-files/base-files.bst` <-- once the elements are cached

These benchmarks are run for every merge commit to BuildStream master.

These measurements are taken via the
[benchmark_debian_project_since.sh](scripts/benchmark_debian_project_since.sh) script.
Which loops over each merge commit since the last measurement (extracted from the
end of the results files). For each commit, a fresh venv is created and BuildStream
is installed.

The [benchmark_locally.py](scripts/benchmark_locally.py) script is used to actually
measure BuildStream's performance at a specific commit.

## The results
The time and max-rss of each BuildStream command is recorded. All time results
are appended to the time_results.csv file. All memory results are appended to
the memory_results.csv file.

## Weekly updates
Weekly updates are emailed to the BuildStream list.

I would recommend copying the whole content of last week's email, then:

- On latest [CI run](https://gitlab.com/BuildStream/benchmarking/benchmark_debian/pipelines), download the artifacts
  Please be careful to put the right numbers in the right columns, it should be pretty intuitive.
- To generate a link for the interactive notebooks:
    - Go to: https://mybinder.org/
    - Copy: "https://gitlab.com/BuildStream/benchmarking/bst-benchmarks-notebooks" into the *"GitLab repo..."* field
    - Enter "master" in the *"Git branch, tag or commit"*, and then all_results.ipynb into the
    *"Path to a notebook file"*, click launch and then copy and paste the link into the email.
