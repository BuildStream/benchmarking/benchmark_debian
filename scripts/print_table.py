#!/usr/bin/env python3

import argparse
import datetime
import csv

from collections import namedtuple

COLLECTED_RESULTS = ["date", "build_4", "build_8", "build_12", "show", "show_cached"]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "type", help="The kind of table to print", choices=["show", "build"]
    )
    parser.add_argument("time", help="The time results file for which to print a table")
    parser.add_argument(
        "memory", help="The memory results file for which to print a table"
    )
    parser.add_argument(
        "-w",
        "--weeks",
        help="The maximum number of previous weeks to print",
        default=5,
        type=int,
    )

    args = parser.parse_args()

    with open(args.time) as csvfile:
        time_results = get_result_weeks(csv.DictReader(csvfile), weeks=args.weeks)
    with open(args.memory) as csvfile:
        memory_results = get_result_weeks(csv.DictReader(csvfile), weeks=args.weeks)

    if args.type == "show":
        print(get_show_string(time_results, memory_results))
    elif args.type == "build":
        print(get_build_string(time_results, memory_results))


def get_result_weeks(results, weeks=5):
    acc = []
    results = reversed(list(results))

    Result = namedtuple("Result", COLLECTED_RESULTS)

    previous = datetime.datetime.max.replace(tzinfo=datetime.timezone.utc)
    # To make sure we don't overflow, we remove 2 weeks
    previous = get_date_range(previous - datetime.timedelta(days=14))
    while len(acc) < weeks:
        try:
            week = next(results)
        except StopIteration:
            break

        date = datetime.datetime.fromisoformat(week["date"])
        date = get_date_range(date)

        # If the date is smaller than the Monday closest to the
        # previous date
        if date[1] < previous[0]:
            previous = date
            week["date"] = datetime.datetime.fromisoformat(week["date"])
            acc.append(Result(*[week[heading] for heading in COLLECTED_RESULTS]))

    return reversed(acc)


def get_date_range(date):
    week_start = date - datetime.timedelta(days=date.weekday())
    week_end = date + datetime.timedelta(days=6 - date.weekday())

    return week_start, week_end


def get_date_string(start, end):
    return "{start:%d}/{start:%m} -> {end:%d}/{end:%m}".format(start=start, end=end)


def get_show_week_string(time_result, memory_result, cell_width):
    return ("│ {:{fill}} " * 5 + "│").format(
        get_date_string(*get_date_range(time_result.date)),
        time_result.show,
        memory_result.show,
        time_result.show_cached,
        memory_result.show_cached,
        fill=cell_width,
    )


def get_build_week_string(time_result, memory_result, cell_width):
    return ("│ {:{fill}} " * 7 + "│").format(
        get_date_string(*get_date_range(time_result.date)),
        time_result.build_4,
        memory_result.build_4,
        time_result.build_8,
        memory_result.build_8,
        time_result.build_12,
        memory_result.build_12,
        fill=cell_width,
    )


def get_build_string(time_results, memory_results):
    return get_string(
        time_results,
        memory_results,
        ["4 Builders", "8 Builders", "12 Builders"],
        get_build_week_string,
    )


def get_show_string(time_results, memory_results):
    return get_string(
        time_results, memory_results, ["Show", "Show once built"], get_show_week_string
    )


def get_string(time_results, memory_results, collection_headings, week_string_callable):
    # This is hairy, but rather generic
    headings = len(collection_headings) * 2 + 1
    cell = len("xx/xx -> xx/xx") + 2
    long_cell = cell * 2 + 1

    top = ("┌{}" + "┬{}" * len(collection_headings) + "┐").format(
        "─" * cell, *["─" * long_cell] * len(collection_headings)
    )

    # The collection headings (e.g. | Builders 4 | Builders 8 |)
    header = "│ {:^{fill}} │".format("", fill=cell - 2)
    header += "│".join(
        (
            "{:^{fill}}".format(heading, fill=long_cell)
            for heading in collection_headings
        )
    )
    header += "│"

    # A separator between the collection and real headings
    header += ("\n├{}" + "┼{}" * len(collection_headings) + "┤").format(
        "─" * cell, *["─" * cell + "┬" + "─" * cell] * len(collection_headings)
    )

    # The real headings (e.g. | Date | Time (s) | Memory (MiB) |)
    header += "\n│ {:^{fill}} │".format("Dates", fill=cell - 2)
    header += " {:^{fill}} │ {:^{fill}} │".format(
        "Time (s)", "Memory (MiB)", fill=cell - 2
    ) * len(collection_headings)

    # A separator between normal table lines
    separator = "\n├{}┤\n".format("┼".join(["─" * cell] * headings))
    header += separator

    table = separator.join(
        week_string_callable(time, memory, cell - 2)
        for time, memory in zip(time_results, memory_results)
    )

    bottom = "└{}┘".format("┴".join(["─" * cell] * headings))

    return top + "\n" + header + table + "\n" + bottom


if __name__ == "__main__":
    main()
