#!/usr/bin/env python3

# benchmark_locally.py: A simple python script which runs `bst show`
# `bst build` (4, 8 and 12 builders) and `bst show` (once elements are cached)
# on a target element. The results are saved to a results file (default: results.json).

import json
import os
import subprocess
import shutil

import click
import numpy as np

os.environ["XDG_CACHE_HOME"] = os.path.join(os.getcwd(), "cache")

@click.command()
@click.option("--repeats", type=int, default=3,
              help='How many times to repeat each test')
@click.option("--target-element", type=str, default="base-files/base-files.bst",
              help='The target element to benchmark')
@click.option('--max-builders', type=click.IntRange(min=0, max=12), default="12",
              help='The maximum number of builders to use in the build jobs.'
                    'This will increment by 4 for each job up to the maximum.')
def benchmark(
        repeats,
        target_element,
        max_builders
):
    show_command = ["bst", "show", target_element]
    build_command = ["bst", "--builders", "4", "build", target_element]

    # Data we want to save
    #
    data = {}
    # show without cache
    data["show_time_measurements"] = []
    data["show_memory_measurements"] = []
    # building (4, 8 and 12 builders)
    data["build_4_time_measurements"] = []
    data["build_4_memory_measurements"] =  []
    data["build_8_time_measurements"] = []
    data["build_8_memory_measurements"] = []
    data["build_12_time_measurements"] = []
    data["build_12_memory_measurements"] = []
    # show with cache
    data["show_cached_time_measurements"] = []
    data["show_cached_memory_measurements"] = []

    # bst show
    measure(show_command,
            data["show_time_measurements"],
            data["show_memory_measurements"],
            repeats
    )

    is_built = False
    # bst build
    if max_builders >= 4:
        build_command[2] = "4"  # This is ugly...
        measure(
            build_command,
            data["build_4_time_measurements"],
            data["build_4_memory_measurements"],
            repeats,
            check_completion=True
        )
        is_built = True

    # bst build (8 builders)
    if max_builders >= 8:
        build_command[2] = "8"
        measure(
            build_command,
            data["build_8_time_measurements"],
            data["build_8_memory_measurements"],
            repeats,
            check_completion=True
        )
        is_built = True


    # bst build (12 builders)
    if max_builders >= 12:
        build_command[2] = "12"
        measure(
            build_command,
            data["build_12_time_measurements"],
            data["build_12_memory_measurements"],
            repeats,
            check_completion=True
        )
        is_built = True


    # show cached
    if is_built:
        measure(
            show_command,
            data["show_cached_time_measurements"],
            data["show_cached_memory_measurements"],
            repeats,
            nuke_cache=False
        )

    # Process results
    time_results = []
    time_results.append(round(np.mean(data["show_time_measurements"]), 2))
    time_results.append(round(np.mean(data["build_4_time_measurements"]), 2))
    time_results.append(round(np.mean(data["build_8_time_measurements"]), 2))
    time_results.append(round(np.mean(data["build_12_time_measurements"]), 2))
    time_results.append(round(np.mean(data["show_cached_time_measurements"]), 2))

    memory_results = []
    memory_results.append(round(np.mean(data["show_memory_measurements"])/1000, 0))
    memory_results.append(round(np.mean(data["build_4_memory_measurements"])/1000, 0))
    memory_results.append(round(np.mean(data["build_8_memory_measurements"])/1000, 0))
    memory_results.append(round(np.mean(data["build_12_memory_measurements"])/1000, 0))
    memory_results.append(round(np.mean(data["show_cached_memory_measurements"])/1000, 0))

    # Dump the data to csv style
    with open("temp_time_results.csv", "w") as outfile:
        time_results_str = ",".join([str(result) for result in time_results])
        outfile.write(time_results_str + "\n")

    with open("temp_memory_results.csv", "w") as outfile:
        memory_results_str = ",".join([str(result) for result in memory_results])
        outfile.write(memory_results_str + "\n")

    # *ALSO* write results to a json file
    results = {}
    results['time'] = {
        "show": time_results[0],
        "build_4": time_results[1],
        "build_8": time_results[2],
        "build_12": time_results[3],
        "show_cached": time_results[4]
    }
    results['memory'] = {
        "show": memory_results[0],
        "build_4": memory_results[1],
        "build_8": memory_results[2],
        "build_12": memory_results[3],
        "show_cached": memory_results[4]
    }

    with open("results.json", "w") as outfile:
        json.dump(results, outfile, indent=4)


###############################
# Utility functions
###############################

def time_buildstream(command, workdir=None, check_completion=False):
    proc = subprocess.run(
        [
            "/usr/bin/time",
            "--format",
            "*** %e %M",
            "bash",
            "-c",
            "{}".format(" ".join(command)),
        ],
        cwd=workdir,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )

    result = {}

    decoded = proc.stdout.decode("utf-8")

    if proc.returncode != 0:
        raise Exception("\n\n*** Error occured:\n{}".format(decoded))

    for line in decoded.split("\n"):
        if check_completion:
            if line.startswith("  Total:"):
                total = int(line.replace(" ", "").split(":")[1])

            if line.startswith("  Build Queue:"):
                processed = int(line.split(",", 1)[0].replace(" ", "").split(":")[1][9:])

        if line.startswith("*** "):
            if check_completion and total != processed:
                print("\tProcessed {}/{}".format(processed, total))
                return {
                    "max_memory": "N/A",
                    "clock_time": "N/A"
                }
            else:
                seconds, max_memory = line.strip("*").strip().split(" ")
                return {
                    "max_memory": round(int(max_memory), 0),
                    "clock_time": round(float(seconds), 2),
                }


def clear_cache():
    cache = os.path.join(os.environ["XDG_CACHE_HOME"], "buildstream")
    if not os.path.exists(cache):
        return
    shutil.rmtree(cache)
    assert not os.path.exists(cache)


def measure(command, times, memories, repeats, nuke_cache=True, check_completion=False):
    print("Benchmarking: " + " ".join(command) + "...")
    for iteration in range(0, repeats + 1):

        if iteration == 0:
            print("\tKernel warmup...")

        if nuke_cache:
            clear_cache()

        result = time_buildstream(command, check_completion=check_completion)

        if iteration == 0:
            print("\t...Done. Commencing benchmark {}/{}".format(iteration+1, repeats))
            continue
        if result["clock_time"] != "N/A":
            times.append(result["clock_time"])
            memories.append(result["max_memory"])
        print("\t{}/{}: {}".format(iteration, repeats, result))

if __name__ == "__main__":
    benchmark()
