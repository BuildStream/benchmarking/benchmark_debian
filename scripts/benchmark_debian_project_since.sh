#!/bin/bash

set -e

LAST_RUN_COMMIT=$1

# First change into the BuildStream directory and ensure we're up to date
cd buildstream

echo "Gathering results for:"
git rev-list --merges --first-parent $LAST_RUN_COMMIT..HEAD

# Now loop through the commits and run the tests for each one
for commit in $(git rev-list --merges --first-parent $LAST_RUN_COMMIT..HEAD | tac)
do
    echo "Running benchmarks for commit $commit"

    # Checkout buildstream at that commit
    git checkout $commit

    ##############################################################
    # Parse git log/show to get information about the merge commit
    ##############################################################

    # Get the commit hash and date
    COMMIT_HASH_AND_DATE=$(git show -s --format='commit: %h, date: %cI' $commit)
    SHORT_HASH=$(git show -s --format='%h' $commit)
    DATE=$(git show -s --format='%cI' $commit)

    BRANCH=$(git log -1 | grep 'Merge branch' | cut -d' ' -f7 | tr -d \')
    MR_number=$(git log -1 | grep 'merge request' | cut -d ! -f2)
    MR="!$MR_number"

    # The commit info in csv
    COMMIT_INFO="$SHORT_HASH,$DATE,$MR,$BRANCH"

    #####################################
    # Install BuildStream at this version into a fresh venv
    #####################################
    # First create a venv and activate it
    echo "Creating new venv..."
    rm -rf benchmarks_venv
    python3 -m venv benchmarks_venv
    source benchmarks_venv/bin/activate
    echo "Upgrading pip..."
    pip -q install --upgrade pip

    # BuildStream dep
    pip3 install cython

    echo "Installing BuildStream..."
    pip3 install -q .
    echo "Done!"

    # Install other non-buildstream deps that we need to run the local benchmarks script
    pip3 install numpy

    #####################################
    # Benchmark locally at the installed version of BuildStream
    #####################################
    cd ../debian-stretch-bst
    python3 -u benchmark_locally.py

    # DEACTIVATE THE VENV
    deactivate

    # Append time results (csv) to COMMIT INFO
    TIME_RESULTS="$COMMIT_INFO,$(cat temp_time_results.csv)"
    echo $TIME_RESULTS >> ../time_results.csv

    MEMORY_RESULTS="$COMMIT_INFO,$(cat temp_memory_results.csv)"
    echo $MEMORY_RESULTS >> ../memory_results.csv

    # Change back to buildstream for the next iteration
    cd ../buildstream

done

# Leave the repo how we found it
git checkout master
